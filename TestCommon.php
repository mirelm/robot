<?php

/**
 * Common test class
 */
class TestCommon extends \PHPUnit_Framework_TestCase
{
	/**
	 * Runs a protected instance method.
	 *
	 * @param object $instance         instance of the object which instance method you want to run
	 * @param string $methodName       name of the method to run
	 * @param array  $methodParameters array of parameters to the method.
	 *
	 * @return mixed
	 */
	protected function callProtectedInstanceMethod($instance, $methodName, array $methodParameters = array())
	{
		$class = new \ReflectionClass($instance);
		$method = $class->getMethod($methodName);
		$method->setAccessible(true);
		return $method->invokeArgs($instance, $methodParameters);
	}

}