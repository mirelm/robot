<?php

include 'RobotGrid.php';
include 'Robot.php';

/**
 * Class responsible for processing the commands and moving the robot on the grid
 */
class RobotRunner
{
	/** available commands for the robot */
	const COMMAND_FORWARD = 'f';
	const COMMAND_BACK = 'b';
	const COMMAND_LEFT = 'l';
	const COMMAND_RIGHT = 'r';

	/** directions on the grid */
	const MOVE_FORWARD = 1;
	const MOVE_BACKWARDS = -1;

	/** possible execution status for the runner */
	const STATUS_SUCCESS = 0;
	const STATUS_OBSTACLE = 1;
	const STATUS_WALL = 2;

	/** @var RobotGrid the grid robot is moving on  */
	private $robotGrid = null;

	/** @var string[] command sequence for the robot */
	private $sequence = [];

	/** @var Robot the robot  */
	private $robot = null;

	/** @var int execution status  */
	private $status = self::STATUS_SUCCESS;

	/**
	 * RobotRunner constructor.
	 *
	 * @param array		$sequence  Command sequence
	 * @param RobotGrid	$robotGrid Robot grid to move on
	 * @param Robot		$robot	   The robot
	 *
	 */
	public function __construct($sequence, $robotGrid, $robot)
	{
		$this->sequence = $sequence;
		$this->robotGrid = $robotGrid;
		$this->robot = $robot;
	}

	/**
	 * run execution
	 *
	 * @return void
	 */
	public function run()
	{
		$this->processCommands($this->sequence);
	}

	/**
	 * get the pair coordinate for the robot
	 *
	 * @return array
	 */
	public function getRobotPosition()
	{
		$robot = $this->getRobot();
		return [$robot->getXCoordinate(), $robot->getYCoordinate()];
	}

	/**
	 * get status message based on the status code
	 *
	 * @return string
	 */
	public function getStatusMessage()
	{
		$code = $this->status;
		$message = '';
		switch ($code) {
			case self::STATUS_SUCCESS:
				$message = 'Run completed successfully.';
				break;
			case self::STATUS_WALL:
				$message = 'Run aborted. Wall encountered.';
				break;
			case self::STATUS_OBSTACLE:
				$message = 'Run aborted. Obstacle encountered.';
				break;
		}

		return $message;
	}

	/**
	 * process given commands
	 *
	 * @param string[] $sequence
	 *
	 * @return void
	 */
	protected function processCommands($sequence)
	{
		$robotGrid = $this->getRobotGrid();

		foreach ($sequence as $command) {

			list($newX, $newY) = $this->calculateNewGridPosition($command);

			if ($robotGrid->isOutOfBound($newX, $newY)) {
				$this->setstatus(self::STATUS_WALL);
				break;
			}

			if ($robotGrid->isObstaclePresent($newX, $newY)) {
				$this->setStatus(self::STATUS_OBSTACLE);
				break;
			}

			$this->getRobot()->setXCoordinate($newX);
			$this->getRobot()->setYCoordinate($newY);
		}
	}

	/**
	 * get the new position on the grid based on the given command
	 *
	 * @param string $command the command to process
	 *
	 * @return array coordinate pair
	 */
	protected function calculateNewGridPosition($command)
	{
		$robot = $this->getRobot();
		switch ($command) {
			case self::COMMAND_LEFT:
				$robot->updateFacingDirection(Robot::LEFT_TURN);
				break;
			case self::COMMAND_RIGHT:
				$robot->updateFacingDirection(Robot::RIGHT_TURN);
				break;
			case self::COMMAND_FORWARD:
				return $this->getNextPosition(self::MOVE_FORWARD);
			case self::COMMAND_BACK:
				return $this->getNextPosition(self::MOVE_BACKWARDS);
		}

		return [$robot->getXCoordinate(), $robot->getYCoordinate()];
	}

	/**
	 * get the coordinate pair for the next move
	 *
	 * @param int $value denoting coordinate increase or decrease on the grid
	 *
	 * @return array
	 */
	protected function getNextPosition($value)
	{
		$robot = $this->getRobot();
		$direction = $robot->getFacingDirection();
		$x = $robot->getXCoordinate();
		$y = $robot->getYCoordinate();

		if ($direction === Robot::EAST) {
			list($newX, $newY) = [$x + $value, $y];
		}

		if ($direction === Robot::WEST) {
			list($newX, $newY) = [$x - $value, $y];
		}

		if ($direction === Robot::SOUTH) {
			list($newX, $newY) = [$x, $y + $value];
		}

		if ($direction === Robot::NORTH) {
			list($newX, $newY) = [$x, $y - $value];
		}

		$grid = $this->getRobotGrid();
		if ($grid->isWrapped()) {
			if ($newX > $grid->getMaxX()) {
				$newX = RobotGrid::MIN_GRID_POSITION;
			} elseif ($newX < RobotGrid::MIN_GRID_POSITION) {
				$newX = $grid->getMaxX();
			} elseif ($newY > $grid->getMaxY()) {
				$newY = RobotGrid::MIN_GRID_POSITION;
			} elseif ($newY < RobotGrid::MIN_GRID_POSITION) {
				$newY = $grid->getMaxY();
			}
		}

		return [$newX, $newY];
	}

	/**
	 * get the robot grid
	 *
	 * @return RobotGrid
	 */
	private function getRobotGrid()
	{
		return $this->robotGrid;
	}

	/**
	 * get the robot
	 *
	 * @return Robot
	 */
	 private function getRobot()
	 {
		return $this->robot;
	 }

	/**
	 * set status of the runner
	 *
	 * @param int $status The status
	 *
	 * @return void
	 */
	 private function setStatus($status)
	 {
		$this->status = $status;
	 }
}