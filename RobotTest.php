<?php

include "Robot.php";
include "TestCommon.php";

/**
 * tests for the Robot class
 */
class RobotTest extends TestCommon
{
	/**
	 * assert that we have correct facing direction depending on different turn commands
	 *
	 * @param string $facingDirection   facing direction
	 * @param string $turn              turn command
	 * @param string $expectedDirection expected direction
	 *
	 * @test
	 * @dataProvider facingDirectionsDataProvider
	 * @return void
	 */
	public function itShouldUpdateFacingDirection($facingDirection, $turn, $expectedDirection)
	{
		$robot = new Robot($facingDirection);
		$robot->updateFacingDirection($turn);
		$this->assertEquals($expectedDirection, $robot->getFacingDirection());
	}

	/**
	 * data provider with different directions and combination of turn commands
	 *
	 * @return string[]
	 */
	public function facingDirectionsDataProvider()
	{
		return [
			['N', 1, 'E'],
			['E', 1, 'S'],
			['S', 1, 'W'],
			['W', 1, 'N'],
			['N', -1, 'W'],
			['E', -1, 'N'],
			['S', -1, 'E'],
			['W', -1, 'S']
		];
	}

	/**
	 * assert that the robot has correct position
	 *
	 * @test
	 * @return void
	 */
	public function itShouldHaveCorrectPosition()
	{
		$robot = new Robot('N');

		$this->assertEquals([0,0], [$robot->getYCoordinate(), $robot->getYCoordinate()]);

		$robot->setXCoordinate(47);
		$robot->setYCoordinate(15);

		$this->assertEquals([47,15], [$robot->getXCoordinate(), $robot->getYCoordinate()]);
	}
}