<?php

include "RobotRunner.php";
include "TestCommon.php";

/**
 * Test class for the RobotGrid class
 */
class RobotRunnerTest extends TestCommon
{

	/**
	 * assert the grid position of the robot
	 *
	 *
	 * @test
	 * @dataProvider nextPositionDataProvider
	 *
	 * @param string $facingDirection facing direction
	 * @param int    $xStart		  start position on x-axis
	 * @param int    $yStart		  start position on y-axis
	 * @param int    $value			  to go forward or backwards
	 * @param int    $expectedX		  where we expect to end up
	 * @param int    $expectedY		  where we expect to end up
	 */
	public function itShouldReturnTheCorrectNextPosition($facingDirection, $xStart, $yStart, $value, $expectedX, $expectedY)
	{
		$robot = new Robot($facingDirection);
		$robot->setStartPosition($xStart, $yStart);

		$grid = new RobotGrid();
		$grid->setWrapped(true);

		$runner = new RobotRunner(['ffff'], $grid, $robot);
		$result = $this->callProtectedInstanceMethod($runner, 'getNextPosition', [$value]);
		$this->assertEquals([$expectedX, $expectedY], $result);
	}

	/**
	 * data provider for the next position test
	 *
	 * @return array
	 */
	public function nextPositionDataProvider()
	{
		return [
			['N', 15, 20, 1, 15, 19],
			['N', 15, 20, -1, 15, 21],
			['E', 15, 20, 1, 16, 20],
			['E', 15, 20, -1, 14, 20],
			['S', 15, 20, 1, 15, 21],
			['S', 15, 20, -1, 15, 19],
			['W', 15, 20, 1, 14, 20],
			['W', 15, 20, -1, 16, 20],

			// Wrapping test cases
			['N', 0, 0, 1, 0, 100],
			['E', 0, 0, -1, 100, 0],
			['W', 0, 100, 1, 100, 100],
			['N', 50, 100, -1, 50, 0],

		];
	}

	/**
	 * @test
	 *
	 * @dataProvider calculatingNewPositionDataProvider
	 *
	 * @param int      $robotStartX
	 * @param int      $robotStartY
	 * @param string   $facingDirection
	 * @param string[] $commands
	 * @param int      $expectedX
	 * @param int      $expectedY
	 */
	public function itShouldCalculateNewGridPositionBasedOnCommand(
		$robotStartX, $robotStartY, $facingDirection, $commands, $expectedX, $expectedY
	) {
		$robot = new Robot($facingDirection);
		$robot->setStartPosition($robotStartX, $robotStartY);
		$grid = new RobotGrid();
		$grid->setObstacle(48, 50);
		$runner = new RobotRunner(str_split($commands), $grid, $robot);
		$this->callProtectedInstanceMethod($runner, 'processCommands', [str_split($commands)]);
		$this->assertEquals([$expectedX, $expectedY], $runner->getRobotPosition());
	}

	/**
	 * data provider for calculating grid position
	 *
	 * @return array
	 */
	public function calculatingNewPositionDataProvider()
	{
		return [
			[0, 0, 'S', 'fflff', 2, 2],
			[1, 1, 'N', 'fflff', 1, 0],
			[50, 50, 'N', 'fflffrbb', 48, 49],
			[0, 0, 'W', 'f', 0, 0]
		];
	}

}