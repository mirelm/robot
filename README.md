Robot challenge

Run it by visiting index.php

Available query parameter settings:

* sequence (Command sequence)
* obstacle (X,Y coordinate of an optional obstacle on the grid)
* facingDirection (Facing direction of the robot)
* startPosition (X,Y coordinate of an optional start position)
* gridSize (Optional grid size)
* wrap (Weather or not the grid should wrap (read robot warp))

Query example:

index.php?sequence=fflffrbb&facingDirection=N&startPosition=50,50&gridSize=100,100&obstacle=48,50&wrap=true