<?php

include "RobotGrid.php";
include "TestCommon.php";

/**
 * Test class for the RobotGrid class
 */
class RobotGridTest extends TestCommon
{

	/**
	 * assert when the coordinate pair is out of the grid
	 *
	 * @param bool $wrapped  if the grid should wrap
	 * @param int  $x        x-coordinate
	 * @param int  $y        y-coordinate
	 * @param bool $expected expected value
	 *
	 * @test
	 * @dataProvider robotGridDataProvider
	 *
	 */
	public function itShouldAssertOutOfGrid($wrapped, $x, $y, $expected)
	{
		$grid = new RobotGrid();
		$grid->setWrapped($wrapped);
		$result = $grid->isOutOfBound($x, $y);
		$this->assertEquals($expected, $result);

	}

	/**
	 * data provider for the grid test
	 *
	 * @return array
	 */
	public function robotGridDataProvider()
	{
		return [
			[false, 100, 100, false],
			[false, 90,14, false],
			[false, 111,1, true],
			[false, 2, 101, true],
			[false, -1, 100, true],
			[true, 111,1, false],
			[true, 2, 101, false],
			[true, -1, 100, false],
		];
	}

	/**
	 * assert correctly when there is an obstacle on the grid
	 *
	 * @test
	 */
	public function itShouldReturnTrueWhenObstaclePresentOnTheGrid()
	{
		$grid = new RobotGrid();
		$grid->setObstacle(5,6);
		$this->assertFalse($grid->isObstaclePresent(5,7));
		$this->assertFalse($grid->isObstaclePresent(6,5));
		$this->assertTrue($grid->isObstaclePresent(5,6));
	}
}