<?php

include "RobotInitializer.php";
include "TestCommon.php";

/**
 * Test class for the RobotInitializer class
 */
class RobotInitializerTest extends TestCommon
{
	/**
	 * assert that we throw an exception when settings are missing
	 *
	 * @test
	 * @expectedException MissingRequiredSettingException
	 *
	 * @return void
	 */
	public function itShouldThrowException()
	{
		$init = new RobotInitializer();
		$init->execute(null);
	}

	/**
	 * assert that we initiate when settings are present
	 *
	 * @test
	 *
	 * @return void
	 */
	public function itShouldExecute()
	{
		$facingDirection = 'N';
		$gridSize = '50,50';
		$obstacle = '30,30';
		$startPosition = '20,20';
		$sequence = 'fffff';
		$wrap = true;

		$settings = [
			RobotInitializer::SETTINGS_PARAM_FACING_DIRECTION => $facingDirection,
			RobotInitializer::SETTINGS_PARAM_GRID_SIZE => $gridSize,
			RobotInitializer::SETTINGS_PARAM_OBSTACLE => $obstacle,
			RobotInitializer::SETTINGS_PARAM_START_POSITION => $startPosition,
			RobotInitializer::SETTINGS_PRAM_SEQUENCE => $sequence,
			RobotInitializer::SETTINGS_PARAM_WRAP => $wrap
		];

		$init = new RobotInitializer();
		$init->execute($settings);

		$result = $this->callProtectedInstanceMethod($init, 'getGridSize', []);
		$this->assertEquals(explode(',', $gridSize), $result);

		$result = $this->callProtectedInstanceMethod($init, 'getFacingDirection', []);
		$this->assertEquals($facingDirection, $result);

		$result = $this->callProtectedInstanceMethod($init, 'getObstaclePosition', []);
		$this->assertEquals(explode(',', $obstacle), $result);

		$result = $this->callProtectedInstanceMethod($init, 'getObstaclePosition', []);
		$this->assertEquals(explode(',', $obstacle), $result);

		$result = $this->callProtectedInstanceMethod($init, 'shouldWrap', []);
		$this->assertTrue($result);
	}

	/**
	 * assert that it runs accordingly when only required settings supplied
	 *
	 * @test
	 * @return void
	 */
	public function itShouldRun()
	{
		$facingDirection = 'N';
		$sequence = 'fflffrbb';

		$settings = [
			RobotInitializer::SETTINGS_PARAM_FACING_DIRECTION => $facingDirection,
			RobotInitializer::SETTINGS_PRAM_SEQUENCE => $sequence
		];

		$init = new RobotInitializer();
		$init->execute($settings);

	}
}