<?php

/**
 * Representation of the grid robot is moving on.
 */
class RobotGrid
{
	/** Start position of the grid */
	CONST MIN_GRID_POSITION = 0;

	/** @var int max number of positions the on x-axis */
	private $maxX = 100;

	/** @var int max number of position on y-axis */
	private $maxY = 100;

	/** @var int the x-coordinate of an obstacle  */
	private $obstacleX = null;

	/** @var int the y-coordinate of an obstacle */
	private $obstacleY = null;

	/** @var bool determines if grid can be wrapped */
	private $wrapped = false;

	/**
	 * set grid size
	 *
	 * @param int $x max number of grids on x-axis
	 * @param int $y max number of grids on y-axis
	 *
	 * @return void
	 */
	public function setGridSize($x, $y)
	{
		$this->maxX = $x;
		$this->maxY = $y;
	}

	/**
	 * place obstacle on the grid
	 *
	 * @param int $x x-coordinate of the obstacle
	 * @param int $y y-coordinate of the obstacle
	 *
	 * @return void
	 */
	public function setObstacle($x, $y)
	{
		$this->obstacleX = $x;
		$this->obstacleY = $y;
	}

	/**
	 * makes the grid wrapped
	 *
	 * @param bool $wrapped
	 */
	public function setWrapped($wrapped)
	{
		$this->wrapped = $wrapped;
	}

	/**
	 * is the grid wrapped
	 *
	 * @return bool
	 */
	public function isWrapped()
	{
		return $this->wrapped;
	}

	/**
	 * get the maximum x-grid position
	 *
	 * @return int
	 */
	public function getMaxX()
	{
		return $this->maxX;
	}

	/**
	 * get the maximum y-grid position
	 *
	 * @return int
	 */
	public function getMaxY()
	{
		return $this->maxY;
	}

	/**
	 * check whether or not the coordinate pair is of the grid
	 *
	 * @param int $x The x-coordinate
	 * @param int $y The y-coordinate
	 *
	 * @return bool
	 *
	 */
	public function isOutOfBound($x, $y)
	{
		if($this->isWrapped()) {
			return false;
		}

		$maxX = $this->getMaxX();
		$maxY = $this->getMaxY();

		if ($x > $maxX || $x < self::MIN_GRID_POSITION || $y > $maxY || $y < self::MIN_GRID_POSITION) {
			return true;
		}

		return false;
	}

	/**
	 * checks if there is an obstacle present
	 *
	 * @param int $x The x-coordinate to check against
	 * @param int $y The y-coordinate to check against
	 *
	 * @return bool
	 */
	public function isObstaclePresent($x, $y)
	{
		$obstacleX = $this->getObstacleX();
		$obstacleY = $this->getObstacleY();

		if ($x === $obstacleX && $y === $obstacleY) {
			return true;
		}

		return false;
	}

	/**
	 * get the x-coordinate for obstacle
	 *
	 * @return int
	 */
	private function getObstacleX()
	{
		return $this->obstacleX;
	}

	/**
	 * get the y-coordinate for obstacle
	 *
	 * @return int
	 */
	private function getObstacleY()
	{
		return $this->obstacleY;
	}
}