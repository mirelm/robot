<?php

/**
 * Representation of the robot.
 */
class Robot
{
	/** available facing directions for the robot */
	const SOUTH = 'S';
	const WEST = 'W';
	const EAST = 'E';
	const NORTH = 'N';

	/** turning directions based on the cardinal directions array */
	const LEFT_TURN = -1;
	const RIGHT_TURN = 1;

	/** @var string facing direction of the robot  */
	private $facingDirection = '';

	/** @var int current x-coordinate for the robot */
	private $positionX = 0;

	/** @var int current y-coordinate for the robot */
	private $positionY = 0;

	/**
	 * robot constructor.
	 *
	 * @param string $facingDirection facing direction of the robot
	 *
	 */
	public function __construct($facingDirection)
	{
		$this->facingDirection = $facingDirection;
	}

	/**
	 * set start position for the robot
	 *
	 * @param int $x x-coordinate for robot start
	 * @param int $y y-coordinate for robot start
	 *
	 * @return void
	 */
	public function setStartPosition($x, $y)
	{
		$this->setXCoordinate($x);
		$this->setYCoordinate($y);
	}

	/**
	 * set the x-coordinate for the robot
	 *
	 * @param int $x The x-coordinate
	 *
	 * @return void
	 */
	public function setXCoordinate($x)
	{
		$this->positionX = $x;
	}

	/**
	 * set y-coordinate of the robot
	 *
	 * @param int $y The y-coordinate
	 *
	 * @return void
	 */
	public function setYCoordinate($y)
	{
		$this->positionY = $y;
	}

	/**
	 * get x-coordinate for the robot
	 *
	 * @return int
	 */
	public function getXCoordinate()
	{
		return $this->positionX;
	}

	/**
	 * get the y-coordinate for the robot
	 *
	 * @return int
	 */
	public function getYCoordinate()
	{
		return $this->positionY;
	}

	/**
	 * get facing direction of the robot
	 *
	 * return string
	 */
	public function getFacingDirection()
	{
		return $this->facingDirection;
	}

	/**
	 * set the facing direction of the robot
	 *
	 * @param int $turn Turn direction
	 *
	 * @return void
	 */
	public function updateFacingDirection($turn)
	{
		$availableDirection = [self::NORTH, self::EAST, self::SOUTH, self::WEST];
		$currentFacingDirection = $this->getFacingDirection();
		$newFacingDirection = $currentFacingDirection;

		foreach ($availableDirection as $key => $direction) {
			if ($direction === $currentFacingDirection) {
				if (isset($availableDirection[$key + $turn])) {
					$newFacingDirection = $availableDirection[$key + $turn];
				} elseif ($turn === self::LEFT_TURN) {
					// Wrap to the end of the array
					$newFacingDirection = self::WEST;
				} else {
					// Wrap to the beginning of the array
					$newFacingDirection = self::NORTH;
				}
			}
		}

		$this->setFacingDirection($newFacingDirection);
	}

	/**
	 * set robot's facing direction
	 *
	 * @param string $direction Facing direction
	 *
	 * @return void
	 */
	private function setFacingDirection($direction)
	{
		$this->facingDirection = $direction;
	}

}