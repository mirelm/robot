<?php

include 'RobotRunner.php';
include 'MissingRequiredSettingException.php';

class RobotInitializer
{
	/** available settings for the runner */
	const SETTINGS_PRAM_SEQUENCE = 'sequence';
	const SETTINGS_PARAM_OBSTACLE = 'obstacle';
	const SETTINGS_PARAM_FACING_DIRECTION = 'facingDirection';
	const SETTINGS_PARAM_START_POSITION = 'startPosition';
	const SETTINGS_PARAM_GRID_SIZE = 'gridSize';
	const SETTINGS_PARAM_WRAP = 'wrap';

	/** @var string[] command sequence */
	private $sequence = [];

	/** @var int[] grid size x,y */
	private $gridSize = [];

	/** @var int[] obstacle coordinate x,y */
	private $obstacle = [];

	/** @var string facing direction */
	private $facingDirection = '';

	/** @var int[] robot's start position */
	private $startPosition = [];

	/** @var bool determines if the grid wraps */
	private $wrap = false;


	/**
	 * execute the command sequence
	 *
	 * @param $query
	 * @return array
	 */
	public function execute($query)
	{
		$this->initiate($query);
		return $this->run();
	}

	/**
	 * run the robot with the provided settings and commands
	 *
	 * @return array get information about the run
	 */
	protected function run()
	{
		$sequence = $this->getCommandSequence();
		$facingDirection = $this->getFacingDirection();

		$robot = new Robot($facingDirection);
		if (!empty($this->getRobotStartPosition())) {
			list($startX, $startY) = $this->getRobotStartPosition();
			$robot->setStartPosition($startX, $startY);
		}

		$grid = new RobotGrid();
		$grid->setWrapped($this->shouldWrap());
		if (!empty($this->getGridSize())) {
			list($gridX, $gridY) = $this->getGridSize();
			$grid->setGridSize($gridX, $gridY);
		}

		if (!empty($this->getObstaclePosition())) {
			list($obstacleX, $obstacleY) = $this->getObstaclePosition();
			$grid->setObstacle($obstacleX, $obstacleY);
		}

		$runner = new RobotRunner($sequence, $grid, $robot);
		$runner->run();

		return $this->getResponseData($runner);
	}

	/**
	 * setup required setting
	 *
	 * @param string[] settings array
	 *
	 * @throws MissingRequiredSettingException
	 * @return void
	 */
	protected function initiate($settings)
	{
		if (!isset($settings[self::SETTINGS_PRAM_SEQUENCE])) {
			throw new MissingRequiredSettingException(self::SETTINGS_PRAM_SEQUENCE);
		} else {
			$this->sequence = str_split($settings[self::SETTINGS_PRAM_SEQUENCE]);
		}

		if (!isset($settings[self::SETTINGS_PARAM_FACING_DIRECTION])) {
			throw new MissingRequiredSettingException(self::SETTINGS_PARAM_FACING_DIRECTION);
		} else {
			$this->facingDirection = $settings[self::SETTINGS_PARAM_FACING_DIRECTION];
		}

		if(isset($settings[self::SETTINGS_PARAM_OBSTACLE])) {
			$this->obstacle = array_map('intval', explode(',', $settings[self::SETTINGS_PARAM_OBSTACLE]));
		}

		if (isset($settings[self::SETTINGS_PARAM_START_POSITION])) {
			$this->startPosition = array_map('intval', explode(',', $settings[self::SETTINGS_PARAM_START_POSITION]));
		}

		if (isset($settings[self::SETTINGS_PARAM_GRID_SIZE])) {
			$this->gridSize = array_map('intval', explode(',', $settings[self::SETTINGS_PARAM_GRID_SIZE]));
		}

		if (isset($settings[self::SETTINGS_PARAM_WRAP]) && $settings[self::SETTINGS_PARAM_WRAP] === true) {
			$this->wrap = true;
		}
	}

	/**
	 * @param RobotRunner $runner
	 *
	 * @return array
	 */
	private function getResponseData($runner)
	{
		list($positionX, $positionY) = $runner->getRobotPosition();
		return [
			'status' => $runner->getStatusMessage(),
			'position' => [
				'X' => $positionX,
				'Y' => $positionY
			]
		];
	}

	/**
	 * get the command sequence
	 *
	 * @return array
	 */
	private function getCommandSequence()
	{
		return $this->sequence;
	}

	/**
	 * get robot's start coordinates
	 *
	 * @return int[]
	 */
	private function getRobotStartPosition()
	{
		return $this->startPosition;
	}

	/**
	 * get obstacle position
	 *
	 * @return int[]
	 */
	private function getObstaclePosition()
	{
		return $this->obstacle;
	}

	/**
	 * get robot's facing direction
	 *
	 * @return string
	 */
	private function getFacingDirection()
	{
		return $this->facingDirection;
	}

	/**
	 * get grid max x and max y
	 *
	 * @return int[]
	 */
	private function getGridSize()
	{
		return $this->gridSize;
	}

	/**
	 * get if the grid should wrap
	 *
	 * @return bool
	 */
	private function shouldWrap()
	{
		return $this->wrap;
	}

}