<?php

/**
 * Class for throwing an exception when required setting is missing
 */
class MissingRequiredSettingException extends \Exception {

	/**
	 * MissingRequiredParameterException constructor.
	 * @param string $parameter
	 */
	public function __construct($parameter)
	{
		$message = "Missing required query parameter: " . $parameter;
		parent::__construct($message);
	}
}