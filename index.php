<?php

include 'RobotInitializer.php';

$init = new RobotInitializer();
try {
	$result = $init->execute($_GET);
} catch (MissingRequiredSettingException $exception) {
	$result = [$exception->getMessage()];
}

header('Content-Type: application/json');
echo json_encode($result);